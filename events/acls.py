import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY

def get_weather_data(city, state):
    params = {
        "q": f"{city},{state},US",
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY,
    }
    url = "http://api.openweathermap.org/geo/1.0/direct"
    response = requests.get(url, params=params)
    content = json.loads(response.content)

    try:
        latitude = content[0]["lat"]
        longitude = content[0]["lon"]
    except (KeyError, IndexError):
        return None

    params = {
        "lat": latitude,
        "lon": longitude,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    url = "https://api.openweathermap.org/data/2.5/weather"
    response = requests.get(url, params=params)
    content = json.loads(response.content)

    try:
        return {
            "description": content["weather"][0]["description"],
            "temp": content["main"]["temp"],
        }
    except (KeyError, IndexError):
        return None


def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "per_page": 1,
        "query": city + " " + state,
    }
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}
# Get the URL of a picture from the Pexels API.
# def get_photo(query):

# # Create a dict for the headers to use in the request
#     headers = {
#         "Authorization": PEXELS_API_KEY
#         }
# # Create URL for the request with the query
#     url = f"https://api.pexels.com/v1/search?query={query}"
# # Make the request
#     response = requests.get(url, headers=headers)
# # Parse the JSON response
#     api_dict = response.json()
# # Return a dictionary contaaining a 'picture_url' key and one of the URLs for one of the pictures in the response
#     return api_dict['photos'][0]['src']['original']


# # Use the Open Weather API
# def get_weather_data(city, state):

# # Get URL for geocoding API with city, state
#     geo_url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state}&appid={OPEN_WEATHER_API_KEY}"
# # Make the request
#     response = requests.get(geo_url)
# # Parse JSON response
#     geoapi_dict = response.json()

# # Get latitude and longitude from the response
#     lat = geoapi_dict["coord"]["lat"]
#     lon = geoapi_dict["coord"]["lon"]

# # Create URL for the current weather API w/ latitude and longitude
#     weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"
# # Make the request
#     weather_response = requests.get(weather_url)
# # Parse the JSON Response
#     weather_dict = weather_response.json()

# # Get main temp and weather's desc. and put into dictionary
#     main_temp = weather_dict["main"]["temp"]
#     weather_desc = weather_dict["weather"][0]["description"]

# # Return the dictionary
#     data_dict = {
#         "temperature": main_temp,
#         "description": weather_desc,
#     }
#     return data_dict
